﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models
{
    public class Product_by_Category
    {
        [Required]
        public int product_id { get; set; }
        public Product Product { get; set; }
        [Required]
        public int category_id { get; set; }
        public Category Category { get; set; }
    }
}

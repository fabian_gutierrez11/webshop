﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Category
    {
        public Category()
        {
            this.Products = new HashSet<Product_by_Category>();
        }
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public virtual IEnumerable<Product_by_Category> Products { get; set; }
    }
}

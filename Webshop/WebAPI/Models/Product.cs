﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Product
    {
        public Product()
        {
            this.Categories = new HashSet<Product_by_Category>();
        }

        public int id { get; set; }
        [Required]
        public string name { get; set; }
        public string description { get; set; }
        [Required]
        public decimal price { get; set; }
        public virtual IEnumerable<Product_by_Category> Categories { get; set; }
    }
}

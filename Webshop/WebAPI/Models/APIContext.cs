﻿using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Models
{
    public class APIContext : DbContext
    {
        public APIContext(DbContextOptions<APIContext> options)
            : base(options)
        {
        }

        public DbSet<Category> Category { get; set; }

        public DbSet<Product> Product { get; set; }

        public DbSet<Product_by_Category> Product_by_Category { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>()
                .HasKey(x => x.id);

            modelBuilder.Entity<Category>()
                .HasKey(x => x.id);

            modelBuilder.Entity<Product_by_Category>()
                .HasKey(x => new { x.product_id, x.category_id });
            modelBuilder.Entity<Product_by_Category>()
                .HasOne(x => x.Product)
                .WithMany(m => m.Categories)
                .HasForeignKey(x => x.product_id);
            modelBuilder.Entity<Product_by_Category>()
                .HasOne(x => x.Category)
                .WithMany(e => e.Products)
                .HasForeignKey(x => x.category_id);
        }
    }


}

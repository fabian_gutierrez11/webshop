﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class Product_by_CategoryController : BaseController
    {
        private readonly APIContext _context;

        public Product_by_CategoryController(APIContext context)
        {
            _context = context;
        }

        // GET: Product_by_Category
        public async Task<IActionResult> Index()
        {
            if (useDatabase)
            {
                var aPIContext = _context.Product_by_Category.Include(p => p.Category).Include(p => p.Product);
                return View(await aPIContext.ToListAsync());
            }
            else
            {
                var aPIContext = ProductCategories;
                return View(aPIContext);
            }
        }


        public async Task<IActionResult> CheckRadio(Microsoft.AspNetCore.Http.IFormCollection frm)
        {
            if (!string.IsNullOrEmpty(frm["selected"].ToString()))
            {
                useDatabase = frm["selected"] == "database" ? true : false;
            }
            return RedirectToAction("Index", "Product_by_Category");
        }


        // GET: Product_by_Category/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            return RedirectToAction("Details", "Products", new { id = id });
        }

        // GET: Product_by_Category/Create
        public IActionResult Create()
        {
            return RedirectToAction("Create", "Products");
        }

    }
}

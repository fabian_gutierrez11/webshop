﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class BaseController : Controller
    {
        public BaseController()
        {
            LoadMemory();
        }

        public static List<Product> Products = new List<Product>();
        public static List<Category> Categories = new List<Category>();
        public static List<Product_by_Category> ProductCategories = new List<Product_by_Category>();
        public static bool useDatabase = true;

        public static void LoadMemory()
        {
            if (Categories.Count == 0)
            {
                Categories.Add(new Category
                {
                    id = Categories.Count + 1,
                    name = "Home Memory",
                    description = "Home Memory"
                });

                Categories.Add(new Category
                {
                    id = Categories.Count + 1,
                    name = "Office Memory",
                    description = "Office Memory"
                });
            }

            if (Products.Count == 0)
            {
                Products.Add(new Product
                {
                    id = Products.Count + 1,
                    name = "TV memory",
                    description = "TV memory"
                });
                Products.Add(new Product
                {
                    id = Products.Count + 1,
                    name = "Computer memory",
                    description = "TV memory"
                });
            }

            if (ProductCategories.Count == 0)
            {
                ProductCategories.Add(new Product_by_Category
                {
                    category_id = 1,
                    product_id = 1,
                    Product = Products.Find(x => x.id == 1),
                    Category = Categories.Find(x => x.id == 1),
                });

                ProductCategories.Add(new Product_by_Category
                {
                    category_id = 2,
                    product_id = 2,
                    Product = Products.Find(x => x.id == 2),
                    Category = Categories.Find(x => x.id == 2)
                });
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class ProductsController : BaseController
    {
        private readonly APIContext _context;

        public ProductsController(APIContext context)
        {
            _context = context;
        }

        // GET: Products
        public async Task<IActionResult> Index()
        {
            return RedirectToAction("Index", "Product_by_Category");
        }

        // GET: Products/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Product
                .SingleOrDefaultAsync(m => m.id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // GET: Products/Create
        public IActionResult Create()
        {
            if (useDatabase)
            {
                ViewData["category_id"] = new SelectList(_context.Category, "id", "name");
                return View();
            }
            else
            {
                ViewData["category_id"] = new SelectList(Categories, "id", "name");
                return View();
            }
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,name,description,price")] Product product)
        {
            if (useDatabase)
            {
                Product_by_Category product_by_Category = new Product_by_Category();
                product_by_Category.category_id = product.id;
                if (ModelState.IsValid && product.id > 0)
                {
                    product.id = 0;
                    _context.Add(product);
                    await _context.SaveChangesAsync();
                    product_by_Category.product_id = _context.Product.ToList().Find(x => x.name == product.name).id;
                    _context.Add(product_by_Category);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index), "Product_by_Category");
                }
                ViewData["category_id"] = new SelectList(_context.Category, "id", "name");
                return View(product);
            }else
            {
                Product_by_Category product_by_Category = new Product_by_Category();
                product_by_Category.category_id = product.id;
                if (ModelState.IsValid && product.id > 0)
                {
                    product.id = 0;
                    Products.Add(product);
                    product_by_Category.product_id = Products.Find(x => x.name == product.name).id;
                    product_by_Category.Product = Products.Find(x => x.id == product_by_Category.product_id);
                    product_by_Category.Category = Categories.Find(x => x.id == product_by_Category.category_id);
                    ProductCategories.Add(product_by_Category);
                    return RedirectToAction(nameof(Index), "Product_by_Category");
                }
                ViewData["category_id"] = new SelectList(_context.Category, "id", "name");
                return View(product);
            }
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Product.SingleOrDefaultAsync(m => m.id == id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Product
                .SingleOrDefaultAsync(m => m.id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }
    }
}
